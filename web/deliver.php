<?php

if (key_exists("date", $_POST) and key_exists("temp", $_POST)) {

    $f = fopen("/tmp/data.txt", "w");
    if ($f == false) {
        die("nah.");
    }

    fwrite($f, json_encode(array(
        "date" => $_POST["date"],
        "temp" => $_POST["temp"]
    )));

    fclose($f);
}

?>
