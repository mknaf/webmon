# About
An experiment to show system performance information on a webpage.

# Requirements
## Server
The server side is currently implemented in `PHP`, so you will need a
webserver with a `PHP` backend.

## Client
No requirements for now.

# Usage
## Server
Host the `web` directory with your favorite webserver.

## Client
The client needs to submit data to the server, for an example see the
script `update.sh`:
```bash
    echo "temp=$( cat /sys/class/hwmon/hwmon0/temp1_input )&date=$( date )" | curl -d @- http://localhost/deliver.php
```
If we run this script as a cronjob, it will automatically be executed
in regular intervals. Edit your crontab by calling `crontab -e` and add
an entry similar to this one:
```text
    */1 * * * * ~/bin/update.sh
```

# Todo
* Might want to make this work with `lm-sensors` package as described
  [here](http://superuser.com/questions/25176/how-can-i-monitor-the-cpu-temperature-under-linux).
* Could make this full-fledged with data collection and presentation
  with graphs and eye candy, using for example [d3js](https://d3js.org/).
